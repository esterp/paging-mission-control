#!/usr/bin/python3

"""
Enlighten IT Consulting Coding Challange - Paging Mission Control

Takes a file with log lines from satellite system and determines if there are Alerts 
given pre-defined parameters. It outputs alerts in a JSON format.

Author: Elkin Sierra
Contact: elkin@sierra.pw
"""
from datetime import datetime, time, timedelta
import argparse
import json

## Configurable Parameters to Follow ##
prmt_keys = ['component-name','seconds-within-instances','number-of-instances', 'direction','safe-limit-parameter']
tstat_alert = ['TSTAT', 300, 3, 1, 'RED HIGH']
batt_alert = ['BATT', 300, 3, -1, 'RED LOW' ]
tstat_alert = dict(zip(prmt_keys, tstat_alert))
batt_alert = dict(zip(prmt_keys, batt_alert))

## log entries to be saved for each component ##
tstat_entries = []
batt_entries = []

def readInputFile(file_name):
    try:
        input_file = open(file_name, 'r')
        lines = input_file.readlines()
        input_file.close()
        return lines
    except IOError:
        print("\nError: File does not appear to exist, please try with a different input file.\n")

# Given a set of log lines return separate lists with only BATT or TSTAT entries
def processData(lines):
    keys = ['timestamp','satellite-id','RED HIGH','YELLOW HIGH','YELLOW LOW','RED LOW','raw-value','component']

    #sort entries by time
    lines.sort()
    
    for line in lines:
        if line != "\n":
            entry_dic = {}
            data = line.replace('\n','').split('|')
            entry_dic = dict(zip(keys, data))
            
            if (entry_dic['component'] == tstat_alert['component-name']):
                tstat_entries.append(entry_dic)
            elif (entry_dic['component'] == batt_alert['component-name']):
                batt_entries.append(entry_dic)

    return tstat_entries, batt_entries

# Given a set of log entries and a set of failure parameters find all entries that would trigger a failure
# and return a list of ALERT(S) if and only if a set # of failures appear in a pre-defined time-lapse (ie: 5 min)
# This generalized function can handle any number of satellites (not just 2) and find any type of alert.
#
# INPUT: Log entries list, and dictionary with alert parameters
# OUTPUT: list of alerts[]
def findAlerts(log_entries, alert_parameters):
    alerts = []
    
    # Dictionary to save the last timestamp seen per satellite-id
    prev_time_by_id = {}
    
    # Number of failures seen within pre-defined time-lapse (ie: 5 min) per each satellite-id
    failures_within_time_lapse_by_id = {}
    
    for entry in log_entries:
        # parse log time stamp into date time object for easier comparasion/manipulation
        entry_date_time = datetime.strptime(entry['timestamp'], '%Y%m%d %H:%M:%S.%f')
        entry['timestamp'] = entry_date_time

        # If this satellite-id doesn't have a previous time stored, it means this ID has not been seen before, add timestamp entry to Dictionary
        if(entry['satellite-id'] not in prev_time_by_id):
            prev_time_by_id.update({ entry['satellite-id'] : entry['timestamp']})

        # If first time seeing satellite-id, FAILURES for this ID should be 0
        if (entry['satellite-id'] not in failures_within_time_lapse_by_id):
            failures_within_time_lapse_by_id.update({ entry['satellite-id'] : 0 })

        # get time difference btw previous and current entry (0 for first line on log file)
        time_diff = abs(prev_time_by_id[entry['satellite-id']] - entry_date_time)
        
        # if previous log entry seen with the same ID is <= to defined time limit (ie: 5 min)
        if(time_diff.total_seconds() <= alert_parameters['seconds-within-instances'] and time_diff.total_seconds() != 0):
            # get safe limit for specific parameter (ie: red low, red high)
            safe_limit = float(alert_parameters['direction'])*float(entry[alert_parameters['safe-limit-parameter']])

            # determine if entry (which we know is within limit) would be a failure and count it
            if not (withinSafeParameter(float(entry['raw-value']), safe_limit)):
                failures_within_time_lapse_by_id[entry['satellite-id']] += 1
        else:
            # outside of time-limit thus is a new time-limit cycle
            failures_within_time_lapse_by_id[entry['satellite-id']] = 0
            
            # check if entry would triggger a FAILURE since it could be the first one on a new cycle
            safe_limit = float(alert_parameters['direction'])*float(entry[alert_parameters['safe-limit-parameter']])
            
            # check if first entry on new cycle would trigger a failure
            if not (withinSafeParameter(float(entry['raw-value']), safe_limit)):
                failures_within_time_lapse_by_id[entry['satellite-id']] += 1
                # save the timestamp in case there are more failures on the cycle and trigger an Alert
                prev_time_by_id.update({ str(entry['satellite-id'])+'_first' : entry['timestamp']})

        # ALERT - If the numbers of FAILURES to reach an ALERT is reached, trigger an Alert
        if (failures_within_time_lapse_by_id[entry['satellite-id']] == alert_parameters['number-of-instances']):
            # Save Alert which consists of: ID, severity, component, first time stamp seen on failure cycle. 
            alerts.append({ "satelliteId":entry['satellite-id'], "severity":alert_parameters['safe-limit-parameter'],
                            'component':alert_parameters['component-name'], 'timestamp':prev_time_by_id[str(entry['satellite-id'])+'_first'] })
            
            # reset the number of failures since an Alert was triggered
            failures_within_time_lapse_by_id[entry['satellite-id']] = 0
            prev_time_by_id.update({ str(entry['satellite-id'])+'_first' : 0}) # not a real need to update it, since it should get overwritten
            
        prev_time_by_id.update({ entry['satellite-id'] : entry['timestamp']})
    return alerts

# check if given raw value falls within safe limit
# (safe limit was flipped to negative in order to simulate 'less than' comparation)
def withinSafeParameter(raw_value, safe_limit):
    # check alert parameter vs actual raw value
    if(raw_value > safe_limit):
        return False
    return True

# Receive dictionary of Alerts and print them as JSON
def printAlertsAsJson (alerts):
    for alert in alerts:
        # update time format
        alert['timestamp'] = alert['timestamp'].isoformat()[:-3]+'Z'
    json_object = json.dumps(alerts, indent = 4)  
    print(json_object) 
    
def main():
    # default input file to be used
    input_file = 'input.txt'
    
    # read -i command line argument and update default file to be used if attribute is provided
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--Input", 
        help = "Use -i argument to provide optional input file otherwise \'input.txt\' would be used.")
    args = parser.parse_args()
    if args.Input:
        input_file = args.Input
    
    alerts = []
    # read input file and parse all lines
    lines = readInputFile(input_file)
    # process all lines and get separate lists for each component (TSTAT, BATT)
    tstat_entries, batt_entries = processData(lines)
    # Find alerts on each component
    alerts.extend(findAlerts(tstat_entries, tstat_alert))
    alerts.extend(findAlerts(batt_entries, batt_alert))
    # Print Alerts
    printAlertsAsJson(alerts)
    
if __name__ == '__main__':
    main();
